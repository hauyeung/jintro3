package a3;
/**
 * 
 * @author Ho Hang Au Yeung
 * @since 2014-04-03
 * Creates full deck of cards
 *
 */
public class FullDeck {
	public Card[] cards = new Card[52];
	public int[] values = {1,2,3,4,5,6,7,8,9,10,11,12,13};
	public String[] ranks = {"Ace","2","3","4","5","6","7","8","9","10","Jack","Queen","King"};
	public String[] suits = {"spade","heart","diamond","clubs"};
	public FullDeck()
	{		
		for (int j=0; j<suits.length; j++)
		{
			for (int i=1; i <= values.length; i++)
			{
				cards[i+13*j-1] = new Card();
				cards[i+13*j-1].setvalue(values[i-1]);
				cards[i+13*j-1].setrank(ranks[i-1]);
				cards[i+13*j-1].setsuit(suits[j]);
			}
		}
	}
	
}
