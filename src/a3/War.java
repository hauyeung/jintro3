package a3;
/**
 * @author Ho Hang Au Yeung
 * Game of War
 */
import java.util.Arrays;
import java.util.Random;

public class War {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FullDeck fdcomp = new FullDeck();
		Card[] compcards = fdcomp.cards;
		FullDeck fdplayer = new FullDeck();
		Card[] playercards = fdplayer.cards;
		int countcompwin = 0;
		int countplayerwin = 0;
		int counttie = 0;
		int y=51;
		while (y >=0)
		{
			Random r = new Random();
			int numcomp = 0;
			int numplayer=0;
			if (y >0)
			{
				numcomp = r.nextInt(y);
				numplayer = r.nextInt(y);
			}
			else
			{
				numcomp = 0;
				numplayer=0;
			}
			if (compcards[numcomp].getvalue() > playercards[numplayer].getvalue())
			{
				System.out.println("Computer wins");
				countcompwin++;
			}
			else if (compcards[numcomp].getvalue() < playercards[numplayer].getvalue())
			{
				System.out.println("Player wins");
				countplayerwin++;
			}
			else
			{
				System.out.println("Tie");
				counttie++;
			}
			fdcomp.cards = removecard(y, numcomp,fdcomp.cards);
			fdplayer.cards = removecard(y,numplayer ,fdplayer.cards);
			y--;
		}
		System.out.println("Computer wins: "+countcompwin);
		System.out.println("Player wins: "+countplayerwin);
		System.out.println("Ties: "+counttie);
		if (countcompwin > countplayerwin)
		{
			System.out.println("Computer wins the game");
		}
		else if (countcompwin < countplayerwin)
		{
			System.out.println("Player wins the game");
		}
		else
		{
			System.out.println("It's a tie");
		}

	}
	/**
	 * 
	 * @param size - size of deck
	 * @param i - index of card to be removed
	 * @param cards - array of cards that needs card removed
	 * @return a deck of card with randomly chosen card removed
	 */
	private static Card[] removecard(int size, int i, Card[] cards)
	{
		if (size >=2)
		{
			Card[] c1 = (Card[]) Arrays.copyOfRange(cards, 0, i);
			Card[] c2 = (Card[]) Arrays.copyOfRange(cards, i+1, size);
			Card[] newdeck = new Card[size-1];
			System.arraycopy(newdeck, 0, c1,0, c1.length);
			System.arraycopy(newdeck, c1.length, c2, 0, c2.length);
			return newdeck;
		}
		else
		{
			return (Card[]) Arrays.copyOfRange(cards, 0, i);
		}
	}

}
