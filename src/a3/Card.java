package a3;
/**
 * 
 * @author Ho Hang Au Yeung
 *@since 2014-04-03
 */
public class Card {
	
	public int value = -1;
	public String rank = null;
	public String suit = null;
	/**
	 * 
	 * @return value of card
	 */
	public int getvalue()
	{
		return value;
	}
	/**
	 * 
	 * @return rank of card
	 */
	public String getrank()
	{
		return rank;
	}
	/**
	 * 
	 * @return suit of card
	 */
	public String getsuit()
	{
		return suit;
	}
	/**
	 * 
	 * @param x value of card
	 */
	public void setvalue(int x)
	{
		value = x;
	}
	
	/**
	 * 
	 * @param x rank of card
	 */
	public void setrank(String x)
	{
		rank = x;
	}
	/**
	 * 
	 * @param x suit of card
	 */
	public void setsuit(String x)
	{
		suit = x;
	}
}
